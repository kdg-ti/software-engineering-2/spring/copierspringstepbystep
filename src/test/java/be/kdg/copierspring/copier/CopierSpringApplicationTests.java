package be.kdg.copierspring.copier;

import be.kdg.copierspring.copier.core.Copier;
import be.kdg.copierspring.copier.core.Reader;
import be.kdg.copierspring.copier.core.Writer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class CopierSpringApplicationTests {

    @Autowired
    Copier copier;


    @Test
    void testExistingCopier() {
        copier.copy();
        assertNotNull(copier);
    }

}
