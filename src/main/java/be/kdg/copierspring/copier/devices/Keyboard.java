package be.kdg.copierspring.copier.devices;

import be.kdg.copierspring.copier.core.Reader;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.Scanner;
@Component
@ConditionalOnProperty(name = "copier.reader", havingValue = "Keyboard")
public class Keyboard implements Reader {

	String heard;
	int counter;

	public Keyboard(){
		System.out.print("I am listening... ");
		Scanner mike = new Scanner("DummyInput0");
		heard =mike.nextLine();
	}
    @Override
    public char read() {
        return heard.charAt(counter++);
    }

    @Override
    public boolean hasNext() {
        return counter < heard.length();
    }


}
