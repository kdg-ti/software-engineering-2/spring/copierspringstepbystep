package be.kdg.copierspring.copier.devices;

import be.kdg.copierspring.copier.core.Writer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name = "copier.writer", havingValue = "Disk")
public class Disk implements Writer {
    @Override
    public void write(char i) {
        System.out.println("Writing " + i + " to disk." );
    }
}
