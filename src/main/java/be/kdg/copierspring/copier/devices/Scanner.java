package be.kdg.copierspring.copier.devices;

import be.kdg.copierspring.copier.core.Reader;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;


@Component
@ConditionalOnProperty(name = "copier.reader", havingValue = "Scanner")
public class Scanner implements Reader {

    @Override
    public char read() {
        return '\u0000';
    }

    @Override
    public boolean hasNext() {
        return false;
    }
}
