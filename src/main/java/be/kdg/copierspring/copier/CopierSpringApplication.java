package be.kdg.copierspring.copier;

import be.kdg.copierspring.copier.core.Copier;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CopierSpringApplication {
    private final Copier copier;

    @Autowired
    public CopierSpringApplication(Copier copier) {
        this.copier = copier;
    }

    public static void main(String[] args) {
                SpringApplication.run(CopierSpringApplication.class, args);
    }
    @PostConstruct
    public void start() {
        copier.copy();
    }

}
