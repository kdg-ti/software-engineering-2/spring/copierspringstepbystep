package be.kdg.copierspring.copier.config;

import be.kdg.copierspring.copier.core.Copier;
import be.kdg.copierspring.copier.core.Reader;
import be.kdg.copierspring.copier.core.Writer;
import be.kdg.copierspring.copier.devices.Keyboard;
import be.kdg.copierspring.copier.devices.Printer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
public class KeyboardToPrinterCopierSpring {

    @Bean
    public Reader reader(){
        return new Keyboard();
    }
    @Bean
    public Writer writer(){
        return new Printer();
    }

    @Bean
    public Copier copier(Reader reader, Writer writer){
        Copier copier = new Copier();
        copier.setReader(reader);
        copier.setWriter(writer);
        return copier;
    }
}
