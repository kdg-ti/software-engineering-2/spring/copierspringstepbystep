package be.kdg.copierspring.copier.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Copier {
	private Reader reader;
	private Writer writer;

	public Copier() {

	}

	public void copy() {

		while (reader.hasNext()) {
			writer.write(reader.read());
		}

	}

	public Reader getReader() {
		return reader;
	}
	@Autowired
	public void setReader(Reader reader) {
		this.reader = reader;
	}

	public Writer getWriter() {
		return writer;
	}
	@Autowired
	public void setWriter(Writer writer) {
		this.writer = writer;
	}
}