package be.kdg.copierspring.copier.core;

public interface Writer {
    public void write(char i);
}
