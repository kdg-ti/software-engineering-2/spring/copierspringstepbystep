package be.kdg.copierspring.copier.core;

public interface Reader {
    public char read();
    public boolean hasNext();
}
